create database TestSSIS

create table AddressType(Id int not null, Name nvarchar(50) not null,
constraint PK_AddressType primary key(Id))

select * from AddressType

update AddressType set Name = '!' where Id = 1
delete AddressType where Id = 2

select * from AddressType


create table ContactType(Id int not null, Name nvarchar(50) not null,
 constraint PK_ContactType primary key(Id))
create table PhoneNumberType(Id int not null, Name nvarchar(50) not null,
 constraint PK_PhoneNumberType primary key(Id))